# craft3-plugin-geolocation plugin for Craft CMS 3.x

A plugin which governs site page access based on a user's geo location.

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require /craft3-plugin-geolocation

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for craft3-plugin-geolocation.

## craft3-plugin-geolocation Overview

-Insert text here-

## Configuring craft3-plugin-geolocation

-Insert text here-

## Using craft3-plugin-geolocation

-Insert text here-

## craft3-plugin-geolocation Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Maurizio Giambrone](http://localhost)
