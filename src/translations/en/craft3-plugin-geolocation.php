<?php
/**
 * craft3-plugin-geolocation plugin for Craft CMS 3.x
 *
 * A plugin which governs site page access based on a user's geo location.
 *
 * @link      http://localhost
 * @copyright Copyright (c) 2020 Maurizio Giambrone
 */

/**
 * craft3-plugin-geolocation en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('craft3-plugin-geolocation', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Maurizio Giambrone
 * @package   Craft3plugingeolocation
 * @since     1.0.0
 */
return [
    'craft3-plugin-geolocation plugin loaded' => 'craft3-plugin-geolocation plugin loaded',
];
